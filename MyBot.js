var stream = require("./lib/stream");
var util = require("util");

var bot = {
	dir: ['N', 'E', 'S', 'W'],
	start: function() {
		var obj = this;
		stream.start();
		stream.on("run", function(){obj.run();stream.flush();});
	},
	run: function() {
		//this.trySomethingALittleBitSmarter();
		this.tryToFillStomach();
	},
	trySomethingALittleBitSmarter: function() {
		var ants = stream.myAnts;

		for (var i=ants.length;i--;) {
			var j = 0, d = 0;
			while(!stream.move(ants[i].id, this.dir[d]) && j < 4) {
				j++;
				d = Math.floor(Math.random() * this.dir.length);
			}
			j = Math.floor(Math.random() * this.dir.length);
			d = this.dir[j];
			this.dir[j] = this.dir[1];
			this.dir[1] = d;
		}
	},
	tryToFillStomach: function() {
		var ants = stream.myAnts;

		for (var i=ants.length;i--;) {
			var k = 0, d = 0, pos = ants[i].pos, q = false, water = [false, false, false, false];

			var go = [
				pos[0] + 1,
				pos[0] - 1,
				pos[1] + 1,
				pos[1] - 1,
			];

			go[0] = go[0] % parseInt(stream.config.rows);
			go[2] = go[2] % parseInt(stream.config.cols);
			while (go[1] < 0) go[1] += parseInt(stream.config.rows);
			while (go[3] < 0) go[3] += parseInt(stream.config.cols);


			if (stream.food.pos([go[0], pos[1]])) continue;
			if (stream.food.pos([go[1], pos[1]])) continue;
			if (stream.food.pos([pos[0], go[2]])) continue;
			if (stream.food.pos([pos[0], go[3]])) continue;

			if (stream.food.pos([go[0], go[2]]) && (stream.move(ants[i].id, 'S', true) || stream.move(ants[i].id, 'E', true))) continue;
			if (stream.food.pos([go[0], go[3]]) && (stream.move(ants[i].id, 'S', true) || stream.move(ants[i].id, 'W', true))) continue;
			if (stream.food.pos([go[1], go[2]]) && (stream.move(ants[i].id, 'N', true) || stream.move(ants[i].id, 'E', true))) continue;
			if (stream.food.pos([go[1], go[3]]) && (stream.move(ants[i].id, 'N', true) || stream.move(ants[i].id, 'W', true))) continue;

			for (var j=1;j++ < 20;) {
				var go = [
					pos[0] + j,
					pos[0] - j,
					pos[1] + j,
					pos[1] - j,
				];

				go[0] = go[0] % parseInt(stream.config.rows);
				go[2] = go[2] % parseInt(stream.config.cols);
				while (go[1] < 0) go[1] += parseInt(stream.config.rows);
				while (go[3] < 0) go[3] += parseInt(stream.config.cols);

				water[0] |= stream.map[go[0]][pos[1]] === -1;
				water[1] |= stream.map[go[1]][pos[1]] === -1;
				water[2] |= stream.map[pos[0]][go[2]] === -1;
				water[3] |= stream.map[pos[0]][go[3]] === -1;

				if (!water[0] && (stream.food.pos([go[0], pos[1]]) || stream.food.pos([go[0], pos[1] - 1]) || stream.food.pos([go[0], pos[1] + 1])) && stream.move(ants[i].id, 'S', true)) {q = true; ants[i].previousMove = 'S';break;};
				if (!water[1] && (stream.food.pos([go[1], pos[1]]) || stream.food.pos([go[1], pos[1] - 1]) || stream.food.pos([go[1], pos[1] + 1])) && stream.move(ants[i].id, 'N', true)) {q = true; ants[i].previousMove = 'N';break;};
				if (!water[2] && (stream.food.pos([pos[0], go[2]]) || stream.food.pos([pos[0] - 1, go[2]]) || stream.food.pos([pos[0] + 1, go[2]])) && stream.move(ants[i].id, 'E', true)) {q = true; ants[i].previousMove = 'E';break;};
				if (!water[3] && (stream.food.pos([pos[0], go[3]]) || stream.food.pos([pos[0] - 1, go[3]]) || stream.food.pos([pos[0] + 1, go[3]])) && stream.move(ants[i].id, 'W', true)) {q = true; ants[i].previousMove = 'W';break;};
			}
			if (q === true) continue;
			if (ants[i].previousMove !== undefined &&
				(Math.floor(Math.random() * 2) === 0) &&
				stream.move(ants[i].id, ants[i].previousMove, true)
			) continue;

			d = Math.floor(Math.random() * this.dir.length);

			while(!stream.move(ants[i].id, this.dir[(d + k) % this.dir.length], true) && k < 4) k++;
			ants[i].previousMove = this.dir[d];
			j = Math.floor(Math.random() * this.dir.length);
			d = this.dir[j];
			this.dir[j] = this.dir[1];
			this.dir[1] = d;
		}
	},
}

bot.start();
