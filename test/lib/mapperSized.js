var assert = require("assert");
var mapper = require("../../lib/mapperSized");
var util = require("util");

// Test basic stuff
console.log("get a new mapper")
var map = new mapper(10, 11);
assert.ok(map.list instanceof Array);
assert.ok(map.map instanceof Array);
assert.strictEqual(10, map.rows);
assert.strictEqual(11, map.cols);
assert.strictEqual(0, map.list.length);
assert.strictEqual(1, map.next);

console.log("add a new value");
map.add({pos:[5,6]});
assert.strictEqual(1, map.list.length);
assert.strictEqual(2, map.next);
assert.strictEqual(5, map.list[0].pos[0]);
assert.strictEqual(6, map.list[0].pos[1]);
assert.strictEqual(1, map.list[0].id);
assert.strictEqual(1, map.map[61].id);

console.log("add another value");
map.add({pos:[3,9]});
assert.strictEqual(2, map.list.length);
assert.strictEqual(3, map.next);
assert.strictEqual(3, map.list[1].pos[0]);
assert.strictEqual(9, map.list[1].pos[1]);
assert.strictEqual(2, map.list[1].id);
assert.strictEqual(2, map.map[42].id);

console.log("add another value");
map.add({pos:[1,2]});
assert.strictEqual(3, map.list.length);
assert.strictEqual(4, map.next);
assert.strictEqual(1, map.list[2].pos[0]);
assert.strictEqual(2, map.list[2].pos[1]);
assert.strictEqual(3, map.list[2].id);
assert.strictEqual(3, map.map[13].id);

console.log("get the second object by id");
var object = map.get(2);
assert.notEqual(false, object);
assert.strictEqual(3, object.pos[0]);
assert.strictEqual(9, object.pos[1]);
assert.strictEqual(2, object.id);

console.log("get the second object by location");
var object = map.pos([3, 9]);
assert.notEqual(false, object);
assert.strictEqual(3, object.pos[0]);
assert.strictEqual(9, object.pos[1]);
assert.strictEqual(2, object.id);

console.log("test lookup");
assert.strictEqual(false, map.lookup(0));
assert.strictEqual(0, map.lookup(1));
assert.strictEqual(1, map.lookup(2));
assert.strictEqual(2, map.lookup(3));

console.log("remove the second object");
map.remove(2);
assert.strictEqual(5, map.list[0].pos[0]);
assert.strictEqual(6, map.list[0].pos[1]);
assert.strictEqual(1, map.list[0].id);
assert.strictEqual(1, map.list[1].pos[0]);
assert.strictEqual(2, map.list[1].pos[1]);
assert.strictEqual(3, map.list[1].id);
assert.strictEqual(1, map.map[61].id);
assert.strictEqual(2, map.list.length);
assert.strictEqual(undefined, map.map[42]);

console.log("add another value to make sure everything still works");
map.add({pos:[7,10]});
assert.strictEqual(3, map.list.length);
assert.strictEqual(5, map.next);
assert.strictEqual(7, map.list[2].pos[0]);
assert.strictEqual(10, map.list[2].pos[1]);
assert.strictEqual(4, map.list[2].id);
assert.strictEqual(4, map.map[87].id);

console.log("add another object");
map2 = new mapper(3, 4);
assert.ok(map2.list instanceof Array);
assert.ok(map2.map instanceof Array);
assert.strictEqual(3, map2.rows);
assert.strictEqual(4, map2.cols);
assert.strictEqual(0, map2.list.length);
assert.strictEqual(1, map2.next);
assert.strictEqual(10, map.rows);
assert.strictEqual(11, map.cols);
assert.strictEqual(3, map.list.length);
assert.strictEqual(5, map.next);

console.log("add a value to our second object");
map2.add({pos:[2,3]});
assert.strictEqual(1, map2.list.length);
assert.strictEqual(2, map2.next);
assert.strictEqual(2, map2.list[0].pos[0]);
assert.strictEqual(3, map2.list[0].pos[1]);
assert.strictEqual(1, map2.list[0].id);
assert.strictEqual(1, map2.map[11].id);

console.log("no errors for retrieve nonexisting objects");
assert.strictEqual(undefined, map.pos([1,1]));
assert.strictEqual(undefined, map.pos([10,10]));

console.log("\ntest completed");
