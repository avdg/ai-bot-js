var events = require('events'),
	mapper = require('./mapperSized');
// Events:
// - config:  Called when configuration settings are passed
//                parameter: Configuration array
// - round:   Called when a round has started (but not processed)
// - run:     Called when a turn is received (where turn > 0);
//                parameter: array of strings with objects
// - cleanup: Called after a flush occurred
// - end:     Called before shutdown
//
// Object specific events:
// - updateMap: Called when a new tile of water has been found
//                  parameter: array with position
// - killed:    Called when an ant gets killed (including enemies)
//                  parameter: object with position (pos) and owner
//
// Specification:
//  - http://aichallenge.org/specification.php#Bot-Input
//  - http://aichallenge.org/specification.php#Bot-Output
//
// Use method move to move a piece
// Objects are raw input lines (see specification or map.js)

// lib/stream.js Shall be the most reliable and efficient code of the project
// It handles incoming data and converts it into the best data it can
module.exports={

	// Stream
	blocked:false, // true if input stream is between turn and ready|go
	emitter:new events.EventEmitter,
	input:[],  // Input cache
	output:"", // Output cache
	stream:{
		stdin: process.stdin,
		stdout: process.stdout,
	},

	// Global settings
	config:{},
	turn:-1,

	// Game state
	map:[], // Separates water, land and unknow, tracks distance from our hills
	players:1, // Player count, including ourself

	ants:null, // Living ants
	myAnts:[], // References to our ants
	theirAnts:[], // References to their ants

	food:null, // Food
	foodSpawn:[], // Food spawned in last round

	hills:[], // Hills

	// Internal state
	circleCache:{},
	moves:[],
	lostAnts:[], // References to lost owned ants
	lostFood:[], // References to lost food

// PUBLIC API //

	on:function(){this.emitter.on.apply(this.emitter, arguments);},
	exit:function(code){process.nextTick(function(){process.exit(code)})},
	pos:function(row,col){
		var arr = [];
		if (row < 0) row += Math.ceil(-row / this.config.rows) * this.config.rows;
		if (col < 0) col += Math.ceil(-col / this.config.cols) * this.config.cols;
		arr[0] = row % this.config.rows;
		arr[1] = col % this.config.cols;
		return arr;
	},

// Ant movements + flush control

	// Set a move for an ant in the buffer
	// @param int id Id of the ant
	// @param string dir Winddirection to go: N, E, S, W
	move: function(id, dir, strict) {
		var dest = this.occupied(id, dir, strict);

		if (!dest) return false;

		// Update directions and collisions data
		this.output += "o " + this.ants.obj[id].pos[0] + " " + this.ants.obj[id].pos[1] + " " + dir + "\n";

		this.ants.obj[id].nextPos = dest;
		this.moves.push(dest);

		return true;
	},
	abortMove: function(id) {
		// Meeeeh!
	},
	flush: function() {
		this.stream.stdout.write(this.output + "go\n");
		this.output = "";
		this.input = [];
		this.moves = [];
		this.objectCleanup();
		this.emitter.emit("cleanup");
	},

// Small general library api

	// Get the tiles at the edge of a circle, doesn't care about overflowing the map
	// pos is a 2d array formatted [x, y], r2 is a squared radius
	// If method equals 'h', will return a half circle pointed to the left
	// If method equals 'v', will return a half circle pointed to the bottom
	// If method equals 'f', it will return a filled circle
	// In other cases, the circle can contain horizontal or vertical duplicates
	getCircle: function(pos, r2, method) {
		// Caches calculation for a half circle
		if (!this.circleCache[r2]) {
			this.circleCache[r2] = [];
			var i = 0, j = Math.ceil(Math.sqrt(r2)|0),
				cache = []; // Cached values to complete the half circle
			var tPos = function(row,col){return[row, col]}; // Optimized storage (if we could :/)

			while (i <= j) {
				var iTemp = i*i;
				var jTemp = j;

				while ((iTemp + j*j) > r2) j--; // Find first point in circle for value i
				this.circleCache[r2].push(tPos(i, j));
				if (i !== 0) this.circleCache[r2].push(tPos(-i, j)); // Oposite side of half circle
				if (jTemp !== j && (i - 1) !== 0) cache.push(tPos(i - 1, jTemp)); // Values to complete the half circle later
				i++;
			}

			// Remove last value if duplicated
			if (
				this.circleCache[r2].length >= 3 &&
				Math.abs(this.circleCache[r2][this.circleCache[r2].length - 2][0]) === Math.abs(this.circleCache[r2][this.circleCache[r2].length - 3][1]) &&
				Math.abs(this.circleCache[r2][this.circleCache[r2].length - 2][1]) === Math.abs(this.circleCache[r2][this.circleCache[r2].length - 3][0])
			) this.circleCache[r2].splice(-2);

			// Complete the half circle
			for (i = cache.length; i--;) {
				this.circleCache[r2].push(tPos(cache[i][0],cache[i][1]));
				this.circleCache[r2].push(tPos(-cache[i][0],cache[i][1]));
			}

			// Do not forget the top and bottom edges of the half circle if necessary
			if (this.circleCache[r2].length < 3 || this.circleCache[r2][this.circleCache[r2].length - 2][1] !== this.circleCache[r2][0][1]) {
				this.circleCache[r2].push(tPos(this.circleCache[r2][0][1],0));
				this.circleCache[r2].push(tPos(-this.circleCache[r2][0][1],0));
			}
		}
		var values = this.circleCache[r2],
			length = values.length,
			cells = [];

		// Calculate the points of the circle from the cache
		switch (method) {
		case 'v':
			for(var i=values.length;i--;)
				cells.push(this.pos(pos[0]+values[i][1],pos[1]+values[i][0]));
			return cells;
		case 'h':
			for(var i=values.length;i--;)
				cells.push(this.pos(pos[0]+values[i][0],pos[1]+values[i][1]));
			return cells;
		case 'f':
			for(var i=values.length;i--;)
				for (var j=values[i][1], l=-j;j >= l;j--)
					cells.push(this.pos(pos[0]+values[i][0],pos[1]+j));
			return cells;
		default:
			for(var i=0;i < length;i++) {
				if (values[i][0] >= values[i][1]) break;
				cells.push(this.pos(pos[0]+values[i][0],pos[1]+values[i][1]));
				cells.push(this.pos(pos[0]+values[i][0],pos[1]-values[i][1]));
				cells.push(this.pos(pos[0]+values[i][1],pos[1]+values[i][0]));
				cells.push(this.pos(pos[0]-values[i][1],pos[1]+values[i][0]));
			}

			// Don't handle this in the loop as i-1 will create duplicates
			if (i >= 0 && values[i][0] === values[i][1]) {
				cells.push(this.pos(pos[0]+values[i][0],pos[1]+values[i][1]));
				cells.push(this.pos(pos[0]+values[i][0],pos[1]-values[i][1]));
				cells.push(this.pos(pos[0]-values[i][1],pos[1]+values[i][0]));
				cells.push(this.pos(pos[0]-values[i][1],pos[1]-values[i][0]));
			}

			return cells;
		}
	},

// Pathfinder

	// Get position from a position and a direction
	destination: function(pos, dir) {
		var row = 0;
		var col = 0;

		if (dir === 'N') row--;
		else if (dir === 'E') col++;
		else if (dir === 'S') row++;
		else if (dir === 'W') col--;

		return this.pos(pos[0] + row, pos[1] + col);
	},
	// Bird distance
	distance: function(pos1, pos2) {
		var dr = Math.min(Math.abs(pos1[0] - pos2[0]), this.config.rows - Math.abs(pos1[0] - pos2[0]));
		var dc = Math.min(Math.abs(pos1[1] - pos2[1]), this.config.cols - Math.abs(pos1[1] - pos2[1]));
		return Math.sqrt((dr * dr) + (dc * dc));
	},
	manhattenDistance: function(pos1, pos2){
		var ver = Math.abs(pos1[0]-pos2[0]);
		var hor = Math.abs(pos1[1]-pos2[1]);
		return
			Math.min(this.config.rows - ver, ver) +
			Math.min(this.config.cols - hor, hor);
	},
	occupied: function(id, dir, strict /* avoid useless tiles */) {
		// Check parameters
		if (["N", "E", "S", "W"].indexOf(dir) === -1) return this.invalidMove(id + " " + dir);
		if (!this.ants.obj[id]) return this.invalidMove(id + " " + dir + " (unknow id)");

		// Destination
		var dest = this.destination(this.ants.obj[id].pos, dir)

		// Check if move is blocked
		if (strict && this.map[dest[0]][dest[1]] < 0) return false;
		else if (this.map[dest[0]][dest[1]] === -1) return false;

		// Check for collisions (no swap :/)
		var obj = this.ants.pos(dest);
		if (
			obj &&
			obj.nextPos &&
			obj.nextPos[0] === dest[0] &&
			obj.nextPos[1] === dest[1]
		) return false;

		// Food collisions
		for (var i=this.foodSpawn.length;i--;)
			if (
				this.foodSpawn[i].pos[0] === dest[0] &&
				this.foodSpawn[i].pos[1] === dest[1]
			) return false;

		// Check if move is reserved
		for (var i=this.moves.length;i--;)
			if (
				dest[0] === this.moves[i][0] &&
				dest[1] === this.moves[i][1]
			) return false;

		return dest;
	},

// Constructor

	// Sets up streams
	start: function() {
		var obj = this;
		this.stream.stdin.resume();
		this.stream.stdin.setEncoding('ascii');
		this.stream.stdin.on('data', function(chunk){return obj.onData(chunk);});
	},

// PRIVATE API //

	invalidMove:function(msg){return false},

// Stream handler

	// Split incoming data in lines and send the data to the right processor
	onData: function(chunk) {
		var lines = chunk.split("\n");
		var length = lines.length;

		for (var i=0;i<length;i++) {
			var line = lines[i].trim();
			if (line === "") continue;
			if (this.turn > 0 || (this.turn === 0 && !this.blocked)) this.process(line);
			else if (this.turn === 0) this.processConfig(line);
			else if (this.turn === -1) this.processInit(line);
		}
	},
	// First call
	processInit: function(line) {
		if (line !== "turn 0") return this.exit(2);
		this.turn = 0;
		this.blocked = true;
		this.emitter.emit("round");
	},
	// First round (round 0 - configuration round)
	processConfig: function(line) {
		if (line === "ready") {
			this.blocked = false;
			this.objectConfigLoaded();
			this.emitter.emit('config', this.config);
			this.flush();
		} else {
			line = line.split(' ');
			if (['loadtime', 'turntime', 'rows', 'cols', 'turns', 'viewradius2', 'attackradius2', 'spawnradius2'].indexOf(line[0]) > -1)
				line[1] = parseInt(line[1]);
			this.config[line[0]] = line[1];
		}
	},
	// Normal rounds (starting from round 1)
	process: function(line) {
		spl = line.split(' ');
		if (!this.blocked) {
			if (spl[0] !== "turn" || spl[1] != (this.turn + 1)) {
				this.turn = -2;
				if (spl[0] === "end") this.emitter.emit("end");
				return this.exit(2);
			}
			this.turn++;
			this.blocked = true;
			this.emitter.emit("round");
			return;
		} else if (spl[0] === "go") {
			this.blocked = false;
			this.objectReady();
			this.emitter.emit("run", this.input);
		} else {
			if (spl.length < 2) return;
			this.processObject(spl);
			this.input.push(line);
		}
	},

// Object handlers

	// Processes config options
	objectConfigLoaded: function() {
		var x = this.config.rows;
		var y = this.config.cols;
		this.ants = new mapper(x, y);
		this.food = new mapper(x, y);
		for(var j=x;j--;) {
			this.map[j]= [];
			for (var i=y;i--;) this.map[j][i] = 0;
		}
	},
	// Preparations for next move
	objectCleanup: function(){
		this.foodCleanup();
		this.antsCleanup();
	},
	// Triggered input gets processed
	objectReady: function(){
		this.antsReady();
		this.foodReady();
	},
	// Processes incoming data
	processObject: function(arr) {
		if (['w', 'a', 'd', 'h', 'f'].indexOf(arr[0]) === -1) return;
		var object={pos:this.pos(parseInt(arr[1]),parseInt(arr[2]))};
		switch(arr[0]){
		case 'w':
			this.map[object.pos[0]][object.pos[1]] = -1;
			this.emitter.emit("updatemap", object.pos);

			// Identify tiles surrounded by 3 water tiles
			var row      = object.pos[0];
			var rowUp    = object.pos[0] - 1;
			var rowDown  = object.pos[0] + 1;
			if (rowDown >= this.config.rows) rowDown = 0;
			else if (rowUp < 0) rowUp = this.config.rows - 1;
			var rowUpUp     = rowUp - 1;
			var rowDownDown = rowDown + 1;
			if (rowDownDown >= this.config.rows) rowDownDown = 0;
			else if (rowUpUp < 0) rowUpUp = this.config.rows - 1;

			var col      = object.pos[1];
			var colRight = object.pos[1] + 1;
			var colLeft  = object.pos[1] - 1;
			if (colRight >= this.config.cols) colRight = 0;
			else if (colLeft < 0) colLeft = this.config.cols - 1;
			var colRightRight = colRight + 1;
			var colLeftLeft   = colLeft - 1;
			if (colRightRight >= this.config.cols) colRightRight = 0;
			else if (colLeftLeft < 0) colLeftLeft = this.config.cols - 1;

			if (this.map[rowDown][colLeft] === -1) {    // down left
				if (this.map[row][colLeft] > -1) {  // left
					if (this.map[rowUp][colLeft] === -1) this.map[row][colLeft] = -2;
					else if (this.map[row][colLeftLeft] === -1) this.map[row][colLeft] = -2;
				}
				if (this.map[rowDown][col] > -1) {  // down
					if (this.map[rowDown][colRight] === -1) this.map[rowDown][col] = -2;
					else if (this.map[rowDownDown][col] === -1) this.map[rowDown][col] = -2;
				}
			}
			if (this.map[rowUp][colRight] === -1) {     // top right
				if (this.map[row][colRight] > -1) { // right
					if (this.map[rowDown][colRight] === -1) this.map[row][colRight] = -2;
					else if (this.map[row][colRightRight] === -1) this.map[row][colRight] = -2;
				}
				if (this.map[rowUp][col] > -1) {    // up
					if (this.map[rowUp][colLeft] === -1) this.map[rowUp][col] = -2;
					else if (this.map[rowUpUp][col] === -1) this.map[rowUp][col] = -2;
				}
			}
			if (this.map[rowDown][colRight] === -1) {   // down right
				if (this.map[row][colRight] > -1)   // right
					if (this.map[row][colRightRight] === -1) this.map[row][colRight] = -2;
				if (this.map[rowDown][col] > -1)    // down
					if (this.map[rowDownDown][col] === -1) this.map[rowDown][col] = -2;
			}
			if (this.map[rowUp][colLeft] === -1) {      // top left
				if (this.map[row][colLeft] > -1)    // left
					if (this.map[row][colLeftLeft] === -1) this.map[row][colLeft] = -2;
				if (this.map[rowUp][col] > -1)      // top
					if (this.map[rowUpUp][col] === -1) this.map[rowUp][col] = -2;
			}
			return;
		case 'a':
			object.owner = parseInt(arr[3]);
			if (object.owner >= this.players) this.players = object.owner + 1;
			return this.antsUpdate(object);
		case 'd':
			object.owner = parseInt(arr[3]);
			if (object.owner >= this.players) this.players = object.owner + 1;
			return this.antsKilled(object);
		case 'h':
			object.owner = parseInt(arr[3]);
			if (object.owner != 0) this.map[object.pos[0]][object.pos[1]] = 0; // Make sure we can reach this tile
			if (object.owner >= this.players) this.players = object.owner + 1;
			return this.hillUpdate(object);
		case 'f':
			return this.foodUpdate(object);
		}
	},

// Ants

	// Validates information we get
	antsUpdate: function(object) {
		// Do we already have this information?
		if (this.ants.pos(object.pos)) {
			this.antsFoundLost(this.ants.pos(object.pos).id);
			return;
		} else {
			var id = this.ants.add(object);
			if (object.owner > 0) {
				this.theirAnts[this.theirAnts.length] = object;
			} else if (object.owner === 0){
				// Add tracking on our ants
				this.myAnts[this.myAnts.length] = object;
				object.nextPos = object.pos.slice(0);
				object.prevPos = object.pos.slice(0);
			}
			return;
		}
	},
	antsReady: function() {
		for(var i=this.lostAnts.length;i--;) {
			this.antRemove(this.lostAnts[i]);
			this.lostAnts.splice(i, 1);
		}
	},
	antsCleanup: function() {
		this.lostAnts = [];

		// TODO Future: garbage collect enemies as well
		for(var i=this.myAnts.length;i--;) {
			// Copy list of our ants, for a check in the next round
			this.lostAnts.push(this.myAnts[i].id);

			// Update positions
			if (
				this.myAnts[i].pos[0] !== this.myAnts[i].nextPos[0] ||
				this.myAnts[i].pos[1] !== this.myAnts[i].nextPos[1]
			) {
				// Update previous and current state of moving ants
				this.myAnts[i].prevPos = this.myAnts[i].pos.slice(0);
				this.ants.update(this.myAnts[i].id, this.myAnts[i].nextPos.slice(0));
			} else if (
				// Update previous state of ants that didn't move
				this.myAnts[i].pos[0] !== this.myAnts[i].prevPos[0] ||
				this.myAnts[i].pos[1] !== this.myAnts[i].prevPos[1]
			) this.myAnts[i].prevPos = this.myAnts[i].pos.slice(0);
		}

		// Clear enemy ants (their might be a better way later)
		for(var i=this.theirAnts.length;i--;) {
			this.ants.remove(this.theirAnts[i].id);
		}

		this.theirAnts = [];
	},
	// We found someone dead on the battlefield, don't mark it as lost and remove it from our ant list
	antsKilled: function(object) {
		var obj = this.ants.pos(object.pos);
		var backup = {pos: [object.pos.slice(0)], owner: object.owner};

		// Notify (we use a backup limited to basic information in case the reference gets lost)
		this.emitter.emit("killed", backup);

		// Remove ant
		if (obj !== undefined) {
			this.antsFoundLost(this.ants.pos(object.pos).id);
			this.antRemove(this.ants.pos(object.pos).id);
		}
	},
	// Make sure we don't mark things we found as lost
	antsFoundLost: function(id) {
		for(var i=this.lostAnts.length;i--;){
			if (this.lostAnts[i] === id) {
				this.lostAnts.splice(i,1);
				return;
			}
		}
	},
	// Remove ants we don't care
	antRemove: function(id){
		// Cleanup friendly ants list first
		if (this.ants.obj[id].owner === 0) {
			for(var j=this.myAnts.length;j--;) {
				if (this.myAnts[j].id === id) {
					this.myAnts.splice(j, 1);
					break;
				}
			}
		}
		this.ants.remove(id);
	},

// Food

	// Validates information we get
	foodUpdate: function(object){
		// Do we already have this information?
		if(this.food.pos(object.pos)) {
			this.foodFoundLost(this.food.pos(object.pos).id);
		} else {
			this.food.add(object);
			this.foodSpawn.push(object);
		}
	},
	foodReady: function(){
		for (var i=this.lostFood.length;i--;) {
			this.foodRemove(this.lostFood[i]);
			this.lostFood.splice(i, 1);
		}
	},
	foodCleanup: function(){
		this.foodSpawn = [];
		this.lostFood = [];

		// Create a list of lost food (suspecting all food)
		for(var i=this.food.list.length;i--;)
			this.lostFood.push(this.food.list[i].id);
	},
	// Make sure we don't mark things we found as lost
	foodFoundLost: function(id){
		for(var i=this.lostFood.length;i--;){
			if (this.lostFood[i] === id) {
				this.lostFood.splice(i,1);
				return;
			}
		}
	},
	// Remove food we don't care
	foodRemove: function(id){
		this.food.remove(id);
	},

// Hills

	// Validates information we get
	hillUpdate: function(object) {
		// Do we already have this information?
		for (var i=this.hills.length;i--;)
			if (
				this.hills[i].pos[0] === object.pos[0] &&
				this.hills[i].pos[1] === object.pos[1]
			) return;

		// Ok, now we know it
		this.hills.push(object);
	},
}