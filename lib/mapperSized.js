/** @constructor */
var mapper = function(rows, cols){
	// Preconditions
	if (!(this instanceof mapper)) return new mapper(rows, cols);
	if (parseInt(rows) !== rows || parseInt(cols) !== cols) return false;

	// Dimensions
	this['rows'] = rows;
	this['cols'] = cols;

	// Lists
	this['list'] = [];
	this['obj'] = {};
	this['map'] = [];

	// Position
	this['next'] = 1; // avoiding 0 that might be equal to false

	// Identifier
	this['id'] = "id";
};
mapper.prototype = {

	constructor: mapper,
	get:function(id){return this.obj[id];},
	length:function(){return this.list.length;},
	pos:function(pos){return this.map[this.hash(pos)];},
	hash:function(pos){return pos[0] * this.cols + pos[1];},

	add: function(data) {
		// Lookup hash
		var hash = this.hash(data.pos);

		// Existance check
		if (this.map[hash]) return false;

		// Define id
		var id = this.next;
		data[this.id] = id;

		// Add objects
		this.list.push(data);
		this.map[hash] = data;
		this.obj[id] = data;

		// Prepare for next id
		this.next++;
		return id;
	},
	remove: function(id) {
		var lookup = this.lookup(id);
		if (lookup === false) return false;

		// Copy by scalar, to avoid lost references
		var pos = [this.obj[id].pos[0], this.obj[id].pos[1]];
		delete this.obj[id];
		this.map[this.hash(pos)] = undefined;
		this.list.splice(lookup, 1);
		this.obj[id] = undefined;
		return true;
	},
	// Looking up for an id in a sorted list, ideal for large lists
	lookup: function(id) {
		if (this.obj[id] === undefined) return false;

		// We know that the position is always smaller than the id
		// We also know that the elements are sorted
		// So we are probably better with binary search in the future
		for(var i=Math.min(id, this.list.length);i--;)
			if (this.list[i].id === id) return i;
		return false;
	},
	// Update the position of an object
	update: function(id, pos) {
		var obj = this.obj[id];
		var oldPos = this.hash(obj.pos);
		var newPos = this.hash(pos);

		if (this.map[newPos] !== undefined) return false;

		obj.pos = pos;
		this.map[newPos] = obj;
		delete this.map[oldPos];
	}
}

module.exports = mapper;
