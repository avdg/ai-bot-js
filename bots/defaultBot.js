var stream = require('../lib/stream');

var bot = {
	dir: ['N', 'E', 'S', 'W'],
	start: function() {
		var obj = this;
		stream.start();
		stream.on("run", function(){obj.run();stream.flush()});
	},
	run: function() {
		var ants = stream.myAnts;
		var dirLength = this.dir.length;

		for (var i=ants.length;i--;)
			for (var j=0; j < dirLength;j++)
				if (stream.move(ants[i].id, this.dir[j])) break;
	},
}

bot.start();
