var stream = require("../lib/stream");

var bot = {
	dir: ['N', 'E', 'S', 'W'],
	start: function() {
		var obj = this;
		stream.start();
		stream.on("run", function(){obj.run();stream.flush()});
	},
	run: function() {
		var ants = stream.myAnts;

		// Move bots to a random direction
		for (var i=ants.length;i--;)
			stream.move(
				ants[i].id,
				this.dir[Math.floor(Math.random()*this.dir.length)]
			);
	}
}

bot.start();
